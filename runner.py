from analysis.mu_sigma_r import run as run_mu_sigma_r
from analysis.inheritance_r_sigma import run as run_inheritance_r_sigma
from analysis.income_tax_full_r_sigma import run as run_income_r_sigma

if __name__ == '__main__':
    run_mu_sigma_r()
    # run_inheritance_r_sigma()
    # run_income_r_sigma()