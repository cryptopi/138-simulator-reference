from functools import partial
from itertools import chain


def constant_full(val):
    return val


def constant(val):
    return partial(constant_full, val)


def linear_full(slope, intercept, variable_name, **kwargs):
    variable = kwargs[variable_name]
    return slope * variable + intercept


def linear(slope, intercept, variable_name):
    return partial(linear_full, slope=slope, intercept=intercept,
                   variable_name=variable_name)
