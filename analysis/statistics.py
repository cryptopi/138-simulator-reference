from itertools import chain
import numpy as np

from simulation.state.simulation_state import SimulationState


def always_compute(iteration):
    return True


def get_talents_and_wealths(state: SimulationState):
    return list(zip(state.talents, state.wealths))


def concat_iterable_results(results):
    return list(chain(*results.values()))


def get_talent_wealth_r(state: SimulationState):
    if np.std(state.wealths) == 0:
        return 0
    return np.corrcoef(state.wealths, state.talents)[0][1]


def get_wealth_variance(state: SimulationState, above_age):
    wealths = state.wealths[state.ages >= above_age]
    return np.var(wealths)


def average_scalar_results(results):
    return np.sum(list(results.values())) / len(results)


def get_income_tax_revenue(state: SimulationState):
    return state.income_tax_revenue


def get_inheritance_tax_revenue(state: SimulationState):
    return state.inheritance_tax_revenue
