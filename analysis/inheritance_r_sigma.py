from functools import partial
from pathlib import Path

from matplotlib.ticker import AutoLocator
from scipy.stats import norm, uniform, randint
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from analysis.derivers import shift_dist_deriver, shift_constant_deriver, \
    fill_constant_deriver, shift_dist_params_deriver, linear_deriver, \
    constant_deriver
from analysis.functions import constant, linear
from analysis.statistics import get_talent_wealth_r, average_scalar_results, \
    get_wealth_variance
from cluster.simulation_cluster import SimulationCluster
from cluster.simulation_job import SimulationJob
from config.breeding.child_deriver import ChildDeriver
from config.breeding.simple_breeding_config import SimpleBreedingConfig
from config.breeding.state_deriver import StateDeriver
from config.continuous_generator import ContinuousGenerator
from config.evolution.asset_compounding_config import AssetCompoundingConfig
from config.evolution.evolution_config import EvolutionConfig
from config.evolution.flat_tax_inheritance_config import \
    FlatTaxInheritanceConfig
from config.evolution.income_tax_config import IncomeTaxConfig
from config.evolution.np_event_wealth_config import NpEventWealthConfig
from config.evolution.paycheck_config import PaycheckConfig
from config.simulation_config import SimulationConfig
from config.state.state_config import StateConfig
from plotting.value_axis_formatter import ValueAxisFormatter
from statistics.runtime_statistic import LambdaStatistic
from utils.utils import is_in, DEBUG, get_poly_fit

paycheck_config = PaycheckConfig(
    shift_dist_params_deriver(
        norm,
        linear_deriver('talents', 20, 0),
        constant_deriver(1),
    )
)
asset_compounding_config = AssetCompoundingConfig(
    get_compound_percents=constant_deriver(0.08)
)
income_tax_config = IncomeTaxConfig(
    start_percentile=0,
    get_tax_rates=constant_deriver(30)
)

# paycheck_config = None
# asset_compounding_config = None
# income_tax_config = None


def get_job(
        death_age,
        generation_gap,
        tax_percentile_start,
        tax_amount,
        amount_index,
        agent_count,
        iteration_count
):
    INITIAL_WEALTH = 10
    breeding_config = SimpleBreedingConfig(
        death_age=death_age,
        generation_gap=generation_gap,
        child_deriver=ChildDeriver(
            state_deriver=StateDeriver(
                derive_talents=shift_dist_deriver(
                    'talents', norm(loc=0, scale=0.01), lower=0, upper=1
                ),
                derive_ages=shift_constant_deriver(
                    'ages', -generation_gap
                ),
                derive_wealths=fill_constant_deriver(INITIAL_WEALTH),
            )
        )
    )
    min_old_age = death_age - generation_gap + 1
    mean_old_age = death_age - generation_gap / 2
    job = SimulationJob(
        'standard',
        SimulationConfig(
            StateConfig(
                initial_wealth_distribution=ContinuousGenerator(INITIAL_WEALTH),
                talent_distribution=ContinuousGenerator(norm(loc=0.6,
                                                             scale=0.2),
                                                        min=0,
                                                        max=1),
                p_unlucky_distribution=ContinuousGenerator(0.03),
                p_lucky_distribution=ContinuousGenerator(0.03),
                age_distribution=ContinuousGenerator(randint(
                    low=min_old_age,
                    high=min_old_age + generation_gap - 1 + 1
                )),
                breeding_config=breeding_config,
                agent_count=agent_count
            ),
            EvolutionConfig(
                event_wealth_config=NpEventWealthConfig(
                    pu_multiply_func=constant(1),
                    pl_multiply_func=linear(1, 0, 'talents'),
                    unlucky_wealth_delta=linear(-1 / 2, 0, 'wealths'),
                    lucky_wealth_delta=linear(1, 0, 'wealths')
                ),
                paycheck_config=paycheck_config,
                asset_compounding_config=asset_compounding_config,
                income_tax_config=income_tax_config,
                inheritance_config=FlatTaxInheritanceConfig(
                    start_percentile=tax_percentile_start,
                    tax_percent=tax_amount,
                    death_age_distribution=ContinuousGenerator(death_age)
                ),
                breeding_config=breeding_config,
                iteration_count=iteration_count
            )
        ),
        statistics=[
            LambdaStatistic(
                'r',
                should_compute_func=partial(is_in, iterable=[iteration_count]),
                compute_func=get_talent_wealth_r,
                aggregate_func=average_scalar_results
            ),
            LambdaStatistic(
                'sigma',
                should_compute_func=partial(is_in, iterable=[iteration_count]),
                compute_func=partial(get_wealth_variance, above_age=20),
                aggregate_func=average_scalar_results
            )
        ],
        params={
            'tax_amount': tax_amount,
            'amount_index': amount_index
        },
        count=1
    )
    return job


def run():
    THREAD_COUNT = 1 if DEBUG else 4
    AGENT_COUNT = 100 if DEBUG else 10000
    ITERATION_COUNT = 100 if DEBUG else 1000
    DEATH_AGE = 81
    GENERATION_GAP = 27
    TAX_PERCENTILE_START = 10
    tax_amounts = [30] if DEBUG else np.linspace(30, 90, 300)

    jobs = []
    for amount_index, tax_amount in enumerate(tax_amounts):
        jobs.append(get_job(
            death_age=DEATH_AGE,
            generation_gap=GENERATION_GAP,
            tax_percentile_start=TAX_PERCENTILE_START,
            tax_amount=tax_amount,
            amount_index=amount_index,
            agent_count=AGENT_COUNT,
            iteration_count=ITERATION_COUNT
        ))

    with SimulationCluster(thread_count=THREAD_COUNT) as cluster:
        results = cluster.submit(*jobs)
        r_job_results = results[('standard', 'r')]
        sigma_job_results = results[('standard', 'sigma')]
        rs = np.zeros(len(tax_amounts))
        sigmas = np.zeros(len(tax_amounts))
        for i in range(len(r_job_results)):
            params = r_job_results[i][0]
            r_stat_results = r_job_results[i][1]
            sigma_stat_results = sigma_job_results[i][1]
            index = params['amount_index']
            rs[index] = r_stat_results[ITERATION_COUNT]
            sigmas[index] = sigma_stat_results[ITERATION_COUNT]

        save_path = Path(
            '~/Desktop/statistics/inheritance_r_sigma'
        ).expanduser()
        np.savez(save_path, r=r_job_results, sigma=sigma_job_results)

        fig, ax1 = plt.subplots()

        ax2 = ax1.twinx()
        ax1.scatter(tax_amounts, rs, color='green')
        ax2.scatter(tax_amounts, sigmas, color='blue')
        sigma_cutoff = 1.1 * np.percentile(sigmas, 95)

        fit_grid = np.linspace(tax_amounts[0], tax_amounts[-1], 1000)
        rs_fit = get_poly_fit(tax_amounts, rs, 2)(fit_grid)
        sigma_fit = np.exp(get_poly_fit(
            tax_amounts[sigmas < sigma_cutoff],
            np.log(sigmas[sigmas < sigma_cutoff]),
            5
        )(fit_grid))
        print(f'Cut off {len(sigmas) - len(sigmas[sigmas < sigma_cutoff])}')
        ax1.plot(fit_grid, rs_fit, color='olive')
        ax2.plot(fit_grid, sigma_fit, color='deepskyblue')

        ax1.set_xlabel('Tax Percent')
        ax1.set_ylabel('R')
        ax2.set_ylabel('Sigma ($\sigma$)')
        ax2.set_ylim((np.min(sigmas) * 0.5, sigma_cutoff * 2))
        ax2.set_yscale('log')

        plt.title('Inheritance Tax Percent vs R and $\sigma$')

        if not DEBUG:
            plt.show()