from functools import partial
from pathlib import Path

from matplotlib.ticker import AutoLocator
from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from analysis.functions import constant, linear
from analysis.statistics import get_talent_wealth_r, average_scalar_results
from cluster.simulation_cluster import SimulationCluster
from cluster.simulation_job import SimulationJob
from config.continuous_generator import ContinuousGenerator
from config.evolution.evolution_config import EvolutionConfig
from config.evolution.np_event_wealth_config import NpEventWealthConfig
from config.simulation_config import SimulationConfig
from config.state.state_config import StateConfig
from plotting.value_axis_formatter import ValueAxisFormatter
from statistics.runtime_statistic import LambdaStatistic
from utils.utils import is_in


def get_job(mu, sigma, mu_index, sigma_index, agent_count, iteration_count):
    job = SimulationJob(
        'standard',
        SimulationConfig(
            StateConfig(
                initial_wealth_distribution=ContinuousGenerator(10),
                talent_distribution=ContinuousGenerator(norm(loc=mu,
                                                             scale=sigma),
                                                        min=0,
                                                        max=1),
                p_unlucky_distribution=ContinuousGenerator(0.01),
                p_lucky_distribution=ContinuousGenerator(0.01),
                age_distribution=ContinuousGenerator(0),
                agent_count=agent_count
            ),
            EvolutionConfig(
                event_wealth_config=NpEventWealthConfig(
                    pu_multiply_func=constant(1),
                    pl_multiply_func=linear(1 / 3, 2 / 3, 'talents'),
                    # pl_multiply_func=linear(0, 1, 'talents'),
                    unlucky_wealth_delta=linear(-1 / 2, 0, 'wealths'),
                    lucky_wealth_delta=linear(1, 0, 'wealths')
                ),
                iteration_count=iteration_count
            )
        ),
        statistics=[
            LambdaStatistic(
                'r2',
                should_compute_func=partial(is_in, iterable=[iteration_count]),
                compute_func=get_talent_wealth_r,
                aggregate_func=average_scalar_results
            )
        ],
        params={
            'mu': mu,
            'sigma': sigma,
            'mu_index': mu_index,
            'sigma_index': sigma_index
        },
        count=1
    )
    return job


def run():
    THREAD_COUNT = 5
    AGENT_COUNT = 20_000
    ITERATION_COUNT = 80
    mus = np.linspace(0.3, 0.8, 30)
    sigmas = np.linspace(0.01, 0.3, 30)

    jobs = []
    for mu_index, mu in enumerate(mus):
        for sigma_index, sigma in enumerate(sigmas):
            jobs.append(get_job(
                mu,
                sigma,
                mu_index,
                sigma_index,
                AGENT_COUNT,
                ITERATION_COUNT
            ))
    with SimulationCluster(thread_count=THREAD_COUNT) as cluster:
        results = cluster.submit(*jobs)
        stat_results = results[('standard', 'r2')]
        r2_grid = np.zeros((len(sigmas), len(mus)))
        for params, job_results in stat_results:
            mu_index = params['mu_index']
            sigma_index = params['sigma_index']
            r2 = job_results[ITERATION_COUNT]
            r2_grid[sigma_index, mu_index] = r2

        save_path = Path('~/Desktop/statistics/wealth_talent_r2').expanduser()
        np.savez(save_path, data=stat_results)

        fig = plt.figure()
        ax = fig.gca()

        # pandas_density = pd.DataFrame(r2_grid, columns=mus, index=sigmas)
        sns.heatmap(r2_grid, ax=ax)
        ax.invert_yaxis()
        ax.xaxis.set_major_locator(AutoLocator())
        ax.xaxis.set_major_formatter(ValueAxisFormatter(mus, '%.2f'))
        ax.yaxis.set_major_formatter(ValueAxisFormatter(sigmas, '%.2f'))
        plt.yticks(rotation=0)
        plt.xlabel('Mu')
        plt.ylabel('Sigma')
        plt.title('Correlation of Luck and Talent')
        plt.show()
