from functools import partial

import numpy as np


def linear_deriver_full(state, member_name, slope, constant):
    values = getattr(state, member_name)
    derived = slope * values + constant
    return derived


def linear_deriver(member_name, slope, constant):
    return partial(
        linear_deriver_full,
        member_name=member_name,
        slope=slope,
        constant=constant
    )


def constant_deriver_full(state, constant):
    return np.repeat(constant, state.agent_count)


def constant_deriver(constant):
    return partial(
        constant_deriver_full,
        constant=constant
    )


def singleton_deriver_full(state, singleton):
    return singleton


def singleton_deriver(singleton):
    return partial(
        singleton_deriver_full,
        singleton=singleton
    )


def none_deriver():
    return singleton_deriver(None)


def shift_dist_params_deriver_full(state, dist, loc_deriver,
                                   scale_deriver,
                                   lower_deriver, upper_deriver):
    locs = loc_deriver(state)
    scales = scale_deriver(state)
    lowers = None if lower_deriver is None else lower_deriver(state)
    uppers = None if upper_deriver is None else upper_deriver(state)
    values = dist.rvs(size=len(locs), loc=locs, scale=scales)
    if lowers is not None:
        values = np.maximum(values, lowers)
    if uppers is not None:
        values = np.minimum(values, uppers)
    return values


def shift_dist_params_deriver(dist, loc_deriver, scale_deriver,
                              lower_deriver=None, upper_deriver=None):
    return partial(
        shift_dist_params_deriver_full,
        dist=dist,
        loc_deriver=loc_deriver,
        scale_deriver=scale_deriver,
        lower_deriver=lower_deriver,
        upper_deriver=upper_deriver
    )


def shift_dist_deriver_full(state, dist, member_name, lower, upper):
    values = getattr(state, member_name)
    values = values + dist.rvs(size=len(values))
    if lower is not None:
        values = np.maximum(values, lower)
    if upper is not None:
        values = np.minimum(values, upper)
    return values


def shift_dist_deriver(member_name, dist, lower=None, upper=None):
    return partial(
        shift_dist_deriver_full,
        dist=dist,
        member_name=member_name,
        lower=lower,
        upper=upper
    )


def shift_constant_deriver_full(state, constant, member_name, lower, upper):
    values = getattr(state, member_name)
    values = values + constant
    if lower is not None:
        values = np.maximum(values, lower)
    if upper is not None:
        values = np.minimum(values, upper)
    return values


def shift_constant_deriver(member_name, constant, lower=None, upper=None):
    return partial(
        shift_constant_deriver_full,
        constant=constant,
        member_name=member_name,
        lower=lower,
        upper=upper
    )


def fill_constant_deriver_full(state, constant):
    return np.repeat(constant, state.agent_count)


def fill_constant_deriver(constant):
    return partial(
        fill_constant_deriver_full,
        constant=constant
    )
