from functools import partial
from pathlib import Path

from matplotlib.ticker import AutoLocator
from scipy.stats import norm, uniform, randint
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from analysis.derivers import shift_dist_deriver, shift_constant_deriver, \
    fill_constant_deriver, shift_dist_params_deriver, linear_deriver, \
    constant_deriver
from analysis.functions import constant, linear
from analysis.statistics import get_talent_wealth_r, average_scalar_results, \
    get_wealth_variance, always_compute, get_income_tax_revenue, \
    get_inheritance_tax_revenue
from cluster.simulation_cluster import SimulationCluster
from cluster.simulation_job import SimulationJob
from config.breeding.child_deriver import ChildDeriver
from config.breeding.simple_breeding_config import SimpleBreedingConfig
from config.breeding.state_deriver import StateDeriver
from config.continuous_generator import ContinuousGenerator
from config.evolution.asset_compounding_config import AssetCompoundingConfig
from config.evolution.evolution_config import EvolutionConfig
from config.evolution.flat_tax_inheritance_config import \
    FlatTaxInheritanceConfig
from config.evolution.income_tax_config import IncomeTaxConfig
from config.evolution.np_event_wealth_config import NpEventWealthConfig
from config.evolution.paycheck_config import PaycheckConfig
from config.simulation_config import SimulationConfig
from config.state.state_config import StateConfig
from plotting.value_axis_formatter import ValueAxisFormatter
from statistics.runtime_statistic import LambdaStatistic
from utils.utils import is_in, DEBUG, get_poly_fit


def get_job(
        income_tax_percentage,
        amount_index,
        death_age,
        generation_gap,
        inheritance_tax_percentile_start,
        inheritance_tax_amount,
        agent_count,
        iteration_count
):
    INITIAL_WEALTH = 10
    breeding_config = SimpleBreedingConfig(
        death_age=death_age,
        generation_gap=generation_gap,
        child_deriver=ChildDeriver(
            state_deriver=StateDeriver(
                derive_talents=shift_dist_deriver(
                    'talents', norm(loc=0, scale=0.01), lower=0, upper=1
                ),
                derive_ages=shift_constant_deriver(
                    'ages', -generation_gap
                ),
                derive_wealths=fill_constant_deriver(INITIAL_WEALTH),
            )
        )
    )
    min_old_age = death_age - generation_gap + 1
    mean_old_age = death_age - generation_gap / 2
    job = SimulationJob(
        'standard',
        SimulationConfig(
            StateConfig(
                initial_wealth_distribution=ContinuousGenerator(INITIAL_WEALTH),
                talent_distribution=ContinuousGenerator(norm(loc=0.6,
                                                             scale=0.2),
                                                        min=0,
                                                        max=1),
                p_unlucky_distribution=ContinuousGenerator(0.03),
                p_lucky_distribution=ContinuousGenerator(0.03),
                age_distribution=ContinuousGenerator(randint(
                    low=min_old_age,
                    high=min_old_age + generation_gap - 1 + 1
                )),
                breeding_config=breeding_config,
                agent_count=agent_count
            ),
            EvolutionConfig(
                event_wealth_config=NpEventWealthConfig(
                    pu_multiply_func=constant(1),
                    pl_multiply_func=linear(1, 0, 'talents'),
                    unlucky_wealth_delta=linear(-1 / 2, 0, 'wealths'),
                    lucky_wealth_delta=linear(1, 0, 'wealths')
                ),
                paycheck_config=PaycheckConfig(
                    shift_dist_params_deriver(
                        norm,
                        linear_deriver('talents', 20, 0),
                        constant_deriver(1),
                    )
                ),
                asset_compounding_config=AssetCompoundingConfig(
                    get_compound_percents=constant_deriver(0.08)
                ),
                income_tax_config=IncomeTaxConfig(
                    start_percentile=0,
                    get_tax_rates=constant_deriver(income_tax_percentage)
                ),
                inheritance_config=FlatTaxInheritanceConfig(
                    start_percentile=inheritance_tax_percentile_start,
                    tax_percent=inheritance_tax_amount,
                    death_age_distribution=ContinuousGenerator(death_age)
                ),
                breeding_config=breeding_config,
                iteration_count=iteration_count
            )
        ),
        statistics=[
            LambdaStatistic(
                'r',
                should_compute_func=partial(is_in, iterable=[iteration_count]),
                compute_func=get_talent_wealth_r,
                aggregate_func=average_scalar_results
            ),
            LambdaStatistic(
                'sigma',
                should_compute_func=partial(is_in, iterable=[iteration_count]),
                compute_func=partial(get_wealth_variance, above_age=20),
                aggregate_func=average_scalar_results
            ),
            LambdaStatistic(
                'income_tax_revenue',
                should_compute_func=partial(is_in, iterable=[iteration_count]),
                compute_func=get_income_tax_revenue,
                aggregate_func=average_scalar_results
            ),
            LambdaStatistic(
                'inheritance_tax_revenue',
                should_compute_func=partial(is_in, iterable=[iteration_count]),
                compute_func=get_inheritance_tax_revenue,
                aggregate_func=average_scalar_results
            )
        ],
        params={
            'income_tax_percentage': income_tax_percentage,
            'amount_index': amount_index
        },
        count=1
    )
    return job


def plot_income_tax_percent_vs_r_sigma(iteration_count,
                                       income_tax_amounts,
                                       r_job_results,
                                       sigma_job_results):
    rs = np.zeros(len(income_tax_amounts))
    sigmas = np.zeros(len(income_tax_amounts))
    for i in range(len(r_job_results)):
        params = r_job_results[i][0]
        r_stat_results = r_job_results[i][1]
        sigma_stat_results = sigma_job_results[i][1]
        index = params['amount_index']
        rs[index] = r_stat_results[iteration_count]
        sigmas[index] = sigma_stat_results[iteration_count]

    save_path = Path(
        '~/Desktop/statistics/income_tax_r_sigma'
    ).expanduser()
    np.savez(save_path, r=r_job_results, sigma=sigma_job_results)

    fig, ax1 = plt.subplots()

    ax2 = ax1.twinx()
    ax1.scatter(income_tax_amounts, rs, color='green')
    ax2.scatter(income_tax_amounts, sigmas, color='blue')
    sigma_cutoff = 1.1 * np.percentile(sigmas, 95)

    fit_grid = np.linspace(income_tax_amounts[0], income_tax_amounts[-1], 1000)
    rs_fit = get_poly_fit(income_tax_amounts, rs, 2)(fit_grid)
    sigma_fit = np.exp(get_poly_fit(
        income_tax_amounts[sigmas < sigma_cutoff],
        np.log(sigmas[sigmas < sigma_cutoff]),
        5
    )(fit_grid))
    print(f'Cut off {len(sigmas) - len(sigmas[sigmas < sigma_cutoff])}')
    ax1.plot(fit_grid, rs_fit, color='olive')
    ax2.plot(fit_grid, sigma_fit, color='deepskyblue')

    ax1.set_xlabel('Income Tax Percent')
    ax1.set_ylabel('R')
    ax2.set_ylabel('Sigma ($\sigma$)')
    ax2.set_ylim((np.min(sigmas) * 0.5, sigma_cutoff * 2))
    ax2.set_yscale('log')

    plt.title('Income Tax Percent vs R and $\sigma$')


def plot_income_tax_percent_vs_revenues(iteration_count,
                                       income_tax_amounts,
                                       income_tax_job_results,
                                       inheritance_tax_job_results):
    income_revenues = np.zeros(len(income_tax_amounts))
    inheritance_revenues = np.zeros(len(income_tax_amounts))
    for i in range(len(income_tax_job_results)):
        params = income_tax_job_results[i][0]
        income_stat_results = income_tax_job_results[i][1]
        inheritance_stat_results = inheritance_tax_job_results[i][1]
        index = params['amount_index']
        income_revenues[index] = income_stat_results[iteration_count]
        inheritance_revenues[index] = inheritance_stat_results[iteration_count]

    save_path = Path(
        '~/Desktop/statistics/income_inheritance_tax_revenues'
    ).expanduser()
    np.savez(save_path,
             income=income_tax_job_results,
             inheritance=inheritance_tax_job_results)

    fig, ax = plt.subplots()

    ax.scatter(income_tax_amounts, income_revenues, color='green',
               label='Income Tax')
    ax.scatter(income_tax_amounts, inheritance_revenues, color='blue',
               label='Inheritance Tax')
    sum_revenue = income_revenues + inheritance_revenues
    ax.scatter(income_tax_amounts, sum_revenue, color='red', label='Total '
                                                                   'Revenue')

    fit_grid = np.linspace(income_tax_amounts[0], income_tax_amounts[-1], 1000)
    income_fit = get_poly_fit(income_tax_amounts, income_revenues, 2)(fit_grid)
    inheritance_fit = get_poly_fit(income_tax_amounts,
                                   inheritance_revenues,
                                   2)(fit_grid)
    ax.plot(fit_grid, income_fit, color='olive')
    ax.plot(fit_grid, inheritance_fit, color='deepskyblue')

    ax.set_xlabel('Income Tax Percent')
    ax.set_ylabel('Revenue')
    ax.set_ylabel('Inheritance Tax Revenue')
    # ax2.set_ylim((np.min(sigmas) * 0.5, sigma_cutoff * 2))
    # ax2.set_yscale('log')

    plt.legend()
    plt.title('Income Tax Percent vs Revenues')


def run():
    THREAD_COUNT = 1 if DEBUG else 4
    AGENT_COUNT = 100 if DEBUG else 1000
    ITERATION_COUNT = 100 if DEBUG else 1000
    DEATH_AGE = 81
    GENERATION_GAP = 27
    INHERITANCE_TAX_AMOUNT = 60
    INHERITANCE_TAX_PERCENTILE_START = 10
    income_tax_amounts = [0.3] if DEBUG else np.linspace(0, 100, 100)

    jobs = []
    for amount_index, income_tax_amount in enumerate(income_tax_amounts):
        jobs.append(get_job(
            income_tax_percentage=income_tax_amount,
            amount_index=amount_index,
            death_age=DEATH_AGE,
            generation_gap=GENERATION_GAP,
            inheritance_tax_percentile_start=INHERITANCE_TAX_PERCENTILE_START,
            inheritance_tax_amount=INHERITANCE_TAX_AMOUNT,
            agent_count=AGENT_COUNT,
            iteration_count=ITERATION_COUNT
        ))

    with SimulationCluster(thread_count=THREAD_COUNT) as cluster:
        results = cluster.submit(*jobs)
        r_job_results = results[('standard', 'r')]
        sigma_job_results = results[('standard', 'sigma')]
        income_job_results = results[('standard', 'income_tax_revenue')]
        inheritance_job_results = results[('standard',
                                           'inheritance_tax_revenue')]
        plot_income_tax_percent_vs_r_sigma(ITERATION_COUNT,
                                           income_tax_amounts,
                                           r_job_results,
                                           sigma_job_results)
        plot_income_tax_percent_vs_revenues(ITERATION_COUNT,
                                            income_tax_amounts,
                                            income_job_results,
                                            inheritance_job_results)

        if not DEBUG:
            plt.show()