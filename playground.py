import os

import numpy as np
import pickle
import time
from scipy.stats import norm, uniform

from multiprocessing import Process, Queue


def read_from_queue(queue):
    while True:
        head = queue.get()
        time.sleep(0.01)


if __name__ == '__main__':
    print()
    # size = 10000000
    # iters = 100
    #
    # start_time = time.time()
    # arr = np.arange(size)
    # for i in range(iters):
    #     pickled = pickle.dumps(arr)
    #     loaded = pickle.loads(pickled)
    # end_time = time.time()
    # per_iter_time = (end_time - start_time) / iters
    # print(f'Time: {per_iter_time}')

    # talents = norm.rvs(size=100)
    # wealths = norm.rvs(size=100)
    #
    # def mutate(talents, wealths):
    #     pass
    #
    # mutate(start)

    # iters = 1000000[
    # start_time = time.time()
    # loc = np.linspace(0, 1, iters)
    # scale = np.repeat(0.1, iters)
    # unif = uniform(loc, scale)
    # unif.rvs(size=iters)
    # end_time = time.time()
    # per_iter_time = (end_time - start_time) / iters
    # print(f'Time: {per_iter_time}')]

    print(os.getpid())

    PROCESS_COUNT = 4
    processes = []
    queue = Queue(maxsize=10)
    for i in range(PROCESS_COUNT):
        processes.append(Process(target=read_from_queue, args=(queue,)))

    for p in processes:
        p.start()

    SIZE = 100000

    def arrays():
        for i in range(1000000):
            yield np.arange(SIZE)

    count = 0
    for payload in arrays():
        queue.put(payload)
        print(f'Added {count}')
        count += 1