import inspect
import functools
import math

import numpy as np
import numpy.polynomial.polynomial as poly


DEBUG = False


def default(val, backup, raw_backup=False):
    if val is not None:
        return val
    if not raw_backup and callable(backup):
        return backup()
    return backup


def absorb(func):
    return functools.partial(apply_absorb, func)


def apply_absorb(zzzfunc, **kwargs):
    parameters = set(inspect.signature(zzzfunc).parameters.keys())

    pass_through = {}
    for name in kwargs:
        if name in parameters or 'kwargs' in parameters:
            pass_through[name] = kwargs[name]
    return zzzfunc(**pass_through)


def apply_state(state, func):
    return func(
        wealths=state.wealths,
        talents=state.talents,
        p_unluckies=state.p_unluckies,
        p_luckies=state.p_luckies
    )


def is_in(value, iterable):
    return value in iterable


def to_chunked_str(arr, length):
    result = ''
    for arr in np.array_split(arr, math.ceil(len(arr) / length)):
        result += str(list(arr)) + '\n'
    return result


def get_poly_fit(x, y, deg):
    coefs = poly.polyfit(x, y, deg)

    def func(nums):
        return poly.polyval(nums, coefs)
    return func
