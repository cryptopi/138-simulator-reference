from functools import partial
from typing import Any, Dict
from uuid import UUID

from simulation.state.simulation_state import SimulationState
from statistics.statistic import Statistic


class LambdaStatistic(Statistic):

    def __init__(self, name, should_compute_func, compute_func, aggregate_func):
        super().__init__(name)
        self.should_compute_func = should_compute_func
        self.compute_func = compute_func
        self.aggregate_func = aggregate_func

    def should_compute(self, iteration):
        return self.should_compute_func(iteration)

    def compute(self, state: SimulationState):
        return self.compute_func(state)

    def aggregate(self, results: Dict[UUID, Any]):
        return self.aggregate_func(results)
