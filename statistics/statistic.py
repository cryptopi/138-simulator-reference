from abc import ABC, abstractmethod
from typing import Dict, Any
from uuid import uuid4, UUID

from simulation.state.simulation_state import SimulationState


class Statistic(ABC):

    def __init__(self, name):
        self.id = uuid4()
        self.name = name

    def should_compute(self, iteration):
        return True

    @abstractmethod
    def compute(self, state: SimulationState):
        pass

    @abstractmethod
    def aggregate(self, results: Dict[UUID, Any]):
        pass
