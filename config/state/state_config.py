from config.breeding.simple_breeding_config import SimpleBreedingConfig
from config.continuous_generator import ContinuousGenerator
from config.breeding.breeding_config import BreedingConfig


class StateConfig:

    def __init__(self,
                 initial_wealth_distribution: ContinuousGenerator,
                 talent_distribution: ContinuousGenerator,
                 p_unlucky_distribution: ContinuousGenerator,
                 p_lucky_distribution: ContinuousGenerator,
                 age_distribution: ContinuousGenerator,
                 agent_count: int,
                 breeding_config: BreedingConfig = None
     ):
        self.initial_wealth_distribution = initial_wealth_distribution
        self.talent_distribution = talent_distribution
        self.p_unlucky_distribution = p_unlucky_distribution
        self.p_lucky_distribution = p_lucky_distribution
        self.age_distribution = age_distribution
        self.breeding_config = breeding_config
        if breeding_config is not None:
            agent_count = breeding_config.clamp_agent_count(agent_count)
        self.agent_count = agent_count

        if isinstance(self.breeding_config, SimpleBreedingConfig):
            mean_age = age_distribution.get_mean()
            max_old_age = self.breeding_config.death_age
            min_old_age = max_old_age - self.breeding_config.generation_gap + 1
            self.age_distribution.min = min_old_age
            self.age_distribution.max = max_old_age
            if not (min_old_age <= mean_age <= max_old_age):
                raise ValueError(f'Mean of age distribution was {mean_age} '
                                 f'but SimpleBreedingConfig was used and so '
                                 f'should be between {min_old_age} and '
                                 f'{max_old_age}')
