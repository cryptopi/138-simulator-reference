from config.breeding.state_deriver import StateDeriver
from simulation.state.simulation_state import SimulationState

import numpy as np


def increment_generation(state: SimulationState):
    return state.generations + 1


def fill_negative_1(state: SimulationState):
    return np.repeat(-1, state.agent_count)


class ChildDeriver:

    def __init__(self, state_deriver: StateDeriver):
        self.state_deriver = state_deriver
        self.state_deriver.derive_generations = increment_generation
        self.state_deriver.derive_children = fill_negative_1

    def derive_children(self, parent_state: SimulationState) -> SimulationState:
        return self.state_deriver.derive_state(parent_state)