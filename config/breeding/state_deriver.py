from simulation.state.simulation_state import SimulationState


class StateDeriver:

    def __init__(
            self,
            derive_wealths=None,
            derive_talents=None,
            derive_p_unluckies=None,
            derive_p_luckies=None,
            derive_generations=None,
            derive_children=None,
            derive_ages=None,
            derive_agent_count=None

    ):
        self.derive_wealths = derive_wealths
        self.derive_talents = derive_talents
        self.derive_p_unluckies = derive_p_unluckies
        self.derive_p_luckies = derive_p_luckies
        self.derive_generations = derive_generations
        self.derive_children = derive_children
        self.derive_ages = derive_ages
        self.derive_agent_count = derive_agent_count

    def apply_deriver(
            self,
            from_state: SimulationState,
            to_state: SimulationState,
            derive_func,
            member_name):
        if derive_func is not None:
            derived = derive_func(from_state)
        else:
            derived = getattr(from_state, member_name)
        setattr(to_state, member_name, derived)

    def derive_state(self, from_state: SimulationState) -> SimulationState:
        derived_state = from_state.copy()
        derive_funcs = [
            self.derive_wealths,
            self.derive_talents,
            self.derive_p_unluckies,
            self.derive_p_luckies,
            self.derive_generations,
            self.derive_children,
            self.derive_ages,
            self.derive_agent_count
        ]
        names = [
            'wealths',
            'talents',
            'p_unluckies',
            'p_luckies',
            'generations',
            'children',
            'ages',
            'agent_count'
        ]
        for name, derive_func in zip(names, derive_funcs):
            self.apply_deriver(
                from_state,
                derived_state,
                derive_func,
                name
            )
        return derived_state
