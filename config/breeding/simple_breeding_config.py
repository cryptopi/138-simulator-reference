from config.breeding.breeding_config import BreedingConfig
from config.breeding.child_deriver import ChildDeriver


class SimpleBreedingConfig(BreedingConfig):

    def __init__(
            self,
            death_age: int,
            generation_gap: int,
            child_deriver: ChildDeriver
    ):
        super().__init__()
        self.death_age = death_age
        self.generation_gap = generation_gap
        self.child_deriver = child_deriver
        self.multiplier = int(self.death_age // self.generation_gap)
        if self.multiplier * self.generation_gap != self.death_age:
            raise ValueError('Generation gap must be multiplier of death age '
                             'for SimpleBreedingConfig')

    def clamp_agent_count(self, agent_count):
        return (agent_count // self.multiplier) * self.multiplier

