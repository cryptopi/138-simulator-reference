from config.evolution.evolution_stage_config import EvolutionStageConfig


class BreedingConfig(EvolutionStageConfig):

    def __init__(self):
        pass

    def clamp_agent_count(self, agent_count):
        return agent_count
