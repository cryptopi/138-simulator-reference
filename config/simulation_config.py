from config.state.state_config import StateConfig
from config.evolution.evolution_config import EvolutionConfig


class SimulationConfig:

    def __init__(self,
                 state_config: StateConfig,
                 evolution_config: EvolutionConfig
    ):
        self.state_config = state_config
        self.evolution_config = evolution_config
