from typing import Union
from scipy.stats import rv_continuous
import numpy as np
import numbers


class ContinuousGenerator:

    def __init__(
            self,
            generator: Union[rv_continuous, float],
            min=None,
            max=None,
            offset=0
    ):
        self.generator = generator
        self.min = min
        self.max = max
        self.is_constant = isinstance(generator, numbers.Number)
        self.offset = offset

    def shifted_by(self, amount):
        return ContinuousGenerator(
            generator=self.generator,
            min=self.min,
            max=self.max,
            offset=self.offset + amount
        )

    def generate(self, count):
        if self.is_constant:
            if count == 1:
                values = self.generator
            else:
                values = np.repeat(self.generator, count)
        else:
            values = self.generator.rvs(size=count)
            if self.min is not None:
                values = np.maximum(values, self.min)
            if self.max is not None:
                values = np.minimum(values, self.max)
        values += self.offset
        return values

    def get_mean(self):
        if self.is_constant:
            return self.generator + self.offset
        else:
            # noinspection PyTypeChecker
            return self.generator.stats('m') + self.offset
