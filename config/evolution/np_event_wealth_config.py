from functools import partial

from config.evolution.event_wealth_config import EventWealthConfig
from utils.utils import absorb


class NpEventWealthConfig(EventWealthConfig):

    def __init__(
            self,
            pu_multiply_func,
            pl_multiply_func,
            unlucky_wealth_delta,
            lucky_wealth_delta
    ):
        self.pu_multiply_func = absorb(pu_multiply_func)
        self.pl_multiply_func = absorb(pl_multiply_func)
        self.unlucky_wealth_delta = absorb(unlucky_wealth_delta)
        self.lucky_wealth_delta = absorb(lucky_wealth_delta)
