from config.continuous_generator import ContinuousGenerator
from config.evolution.inheritance_config import InheritanceConfig


class FlatTaxInheritanceConfig(InheritanceConfig):

    def __init__(
            self,
            start_percentile: float,
            tax_percent: float,
            death_age_distribution: ContinuousGenerator
    ):
        super().__init__()
        self.start_percentile = start_percentile
        self.tax_percent = tax_percent
        self.death_age_distribution = death_age_distribution

    def get_death_ages(self, count):
        return self.death_age_distribution.generate(count)
