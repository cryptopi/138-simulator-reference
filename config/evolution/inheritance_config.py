from abc import abstractmethod

from config.evolution.evolution_stage_config import EvolutionStageConfig


class InheritanceConfig(EvolutionStageConfig):

    def __init__(self):
        pass

    @abstractmethod
    def get_death_ages(self, count):
        pass