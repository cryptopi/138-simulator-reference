from config.evolution.evolution_stage_config import EvolutionStageConfig


class PaycheckConfig(EvolutionStageConfig):

    def __init__(self, paycheck_from_state):
        self.paycheck_from_state = paycheck_from_state
