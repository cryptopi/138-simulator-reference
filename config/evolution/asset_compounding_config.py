from config.evolution.evolution_stage_config import EvolutionStageConfig


class AssetCompoundingConfig(EvolutionStageConfig):

    def __init__(self, get_compound_percents):
        self.get_compound_percents = get_compound_percents
