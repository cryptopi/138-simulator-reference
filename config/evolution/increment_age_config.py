from config.evolution.evolution_stage_config import EvolutionStageConfig


class IncrementAgeConfig(EvolutionStageConfig):

    def __init__(self, amount):
        self.amount = amount