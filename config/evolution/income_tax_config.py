from config.evolution.evolution_stage_config import EvolutionStageConfig


class IncomeTaxConfig(EvolutionStageConfig):

    def __init__(self, start_percentile, get_tax_rates):
        self.start_percentile = start_percentile
        self.get_tax_rates = get_tax_rates
