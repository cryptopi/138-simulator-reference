from typing import List

from .asset_compounding_config import AssetCompoundingConfig
from .event_wealth_config import EventWealthConfig
from .evolution_stage_config import EvolutionStageConfig
from .income_tax_config import IncomeTaxConfig
from .increment_age_config import IncrementAgeConfig
from .inheritance_config import InheritanceConfig
from .paycheck_config import PaycheckConfig
from ..breeding.breeding_config import BreedingConfig


class EvolutionConfig:

    def __init__(self,
                 event_wealth_config: EventWealthConfig,
                 iteration_count: int,
                 paycheck_config: PaycheckConfig = None,
                 asset_compounding_config: AssetCompoundingConfig = None,
                 income_tax_config: IncomeTaxConfig = None,
                 inheritance_config: InheritanceConfig = None,
                 breeding_config: BreedingConfig = None
                 # paycheck_config: PaycheckConfig,
                 # luck_mutate_config: LuckMutateConfig,
                 # redistribution_config: RedistributionConfig
     ):
        self.event_wealth_config = event_wealth_config
        self.paycheck_config = paycheck_config
        self.asset_compounding_config = asset_compounding_config
        self.income_tax_config = income_tax_config
        self.inheritance_config = inheritance_config
        self.breeding_config = breeding_config
        # self.paycheck_config = paycheck_config
        # self.luck_mutate_config = luck_mutate_config
        # self.redistribution_config = redistribution_config
        self.increment_age_config = IncrementAgeConfig(1)
        self.iteration_count = iteration_count

    def get_stage_configs(self) -> List[EvolutionStageConfig]:
        stages = [
            self.event_wealth_config,
            self.paycheck_config,
            self.asset_compounding_config,
            self.income_tax_config,
            self.inheritance_config,
            self.breeding_config,
            self.increment_age_config
        ]
        return [stage for stage in stages if stage is not None]
