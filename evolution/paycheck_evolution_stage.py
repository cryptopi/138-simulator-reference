from config.evolution.paycheck_config import PaycheckConfig
from evolution.evolution_stage import EvolutionStage
from simulation.state.simulation_state import SimulationState
from simulation.state.state_update import StateUpdate


class PaycheckEvolutionStage(EvolutionStage):

    def __init__(self, config: PaycheckConfig):
        super().__init__()
        self.config = config

    def get_update(self, state: SimulationState, iteration: int) -> StateUpdate:
        paycheck_amounts = self.config.paycheck_from_state(state)
        return StateUpdate(
            wealths=paycheck_amounts
        )