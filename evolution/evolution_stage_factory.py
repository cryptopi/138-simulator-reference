from config.breeding.simple_breeding_config import SimpleBreedingConfig
from config.evolution.asset_compounding_config import AssetCompoundingConfig
from config.evolution.evolution_stage_config import EvolutionStageConfig
from config.evolution.flat_tax_inheritance_config import \
    FlatTaxInheritanceConfig
from config.evolution.income_tax_config import IncomeTaxConfig
from config.evolution.increment_age_config import IncrementAgeConfig
from config.evolution.np_event_wealth_config import NpEventWealthConfig
from config.evolution.paycheck_config import PaycheckConfig
from .asset_compounding_stage import AssetCompoundingStage

from .evolution_stage import EvolutionStage
from .flat_tax_inheritance_stage import FlatTaxInheritanceStage
from .income_tax_stage import IncomeTaxStage
from .increment_age_stage import IncrementAgeStage
from .np_wealth_evolution_stage import NpEventWealthStage
from .paycheck_evolution_stage import PaycheckEvolutionStage
from .simple_breeding_stage import SimpleBreedingStage


class EvolutionStageFactory:

    def __init__(self):
        pass

    def create(self, stage_config: EvolutionStageConfig) -> EvolutionStage:
        if isinstance(stage_config, NpEventWealthConfig):
            return NpEventWealthStage(stage_config)
        if isinstance(stage_config, SimpleBreedingConfig):
            return SimpleBreedingStage(stage_config)
        if isinstance(stage_config, FlatTaxInheritanceConfig):
            return FlatTaxInheritanceStage(stage_config)
        if isinstance(stage_config, IncrementAgeConfig):
            return IncrementAgeStage(stage_config)
        if isinstance(stage_config, PaycheckConfig):
            return PaycheckEvolutionStage(stage_config)
        if isinstance(stage_config, AssetCompoundingConfig):
            return AssetCompoundingStage(stage_config)
        if isinstance(stage_config, IncomeTaxConfig):
            return IncomeTaxStage(stage_config)

        raise ValueError(f'Stage config {type(stage_config)} is not '
                         f'recognized.')
