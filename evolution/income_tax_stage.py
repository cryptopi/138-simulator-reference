from config.evolution.income_tax_config import IncomeTaxConfig
from evolution.evolution_stage import EvolutionStage
from simulation.state.simulation_state import SimulationState
from simulation.state.state_update import StateUpdate


class IncomeTaxStage(EvolutionStage):

    def __init__(self, config: IncomeTaxConfig):
        super().__init__()
        self.config = config

    def get_update(self, state: SimulationState, iteration: int) -> StateUpdate:
        if self.config.start_percentile != 0:
            print(f'WARNING: can only tax starting at 0 percentile for now')
        tax_rates = self.config.get_tax_rates(state) / 100
        return StateUpdate(
            income_tax_rates=tax_rates
        )