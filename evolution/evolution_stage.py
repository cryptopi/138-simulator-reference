from abc import ABC, abstractmethod
from simulation.state.simulation_state import SimulationState
from simulation.state.state_update import StateUpdate


class EvolutionStage(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def get_update(self, state: SimulationState, iteration: int) -> StateUpdate:
        pass
