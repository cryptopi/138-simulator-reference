from config.breeding.simple_breeding_config import SimpleBreedingConfig
from evolution.evolution_stage import EvolutionStage
from simulation.state.simulation_state import SimulationState
from simulation.state.state_update import StateUpdate

import numpy as np

class SimpleBreedingStage(EvolutionStage):

    def __init__(self, config: SimpleBreedingConfig):
        super().__init__()
        self.config = config

    def get_update(self, state: SimulationState, iteration: int) -> StateUpdate:
        at_death_age = state.ages == self.config.death_age
        new_parents = state.ages == self.config.generation_gap
        parent_state = state.slice(new_parents)
        child_state = self.config.child_deriver.derive_children(parent_state)
        if child_state.agent_count != np.sum(at_death_age):
            raise ValueError('Child state not equal to death age')
        return StateUpdate(
            state_replace=child_state,
            parent_indices=new_parents,
            indices=at_death_age
        )