from scipy.stats import uniform
import numpy as np

from config.evolution.np_event_wealth_config import NpEventWealthConfig
from evolution.evolution_stage import EvolutionStage
from simulation.state.simulation_state import SimulationState
from simulation.state.state_update import StateUpdate
from utils.utils import apply_state


class NpEventWealthStage(EvolutionStage):

    def __init__(self, config: NpEventWealthConfig):
        super().__init__()
        self.config = config
        self.uniform = uniform()

    def get_update(self, state: SimulationState, iteration: int) -> StateUpdate:
        pus = state.p_unluckies * apply_state(state, self.config.pu_multiply_func)
        pus = np.maximum(pus, 0)
        pus = np.minimum(pus, 1)
        pls = state.p_unluckies * apply_state(state, self.config.pl_multiply_func)
        pls = np.maximum(pls, 0)
        pls = np.minimum(pls, 1 - pus)
        probs = self.uniform.rvs(size=state.agent_count)
        unlucky_indices = probs < pus
        lucky_indices = (pus <= probs) & (probs <= pus + pls)
        unlucky_amounts = apply_state(state, self.config.unlucky_wealth_delta)
        lucky_amounts = apply_state(state, self.config.lucky_wealth_delta)
        wealth_deltas = np.zeros(state.agent_count)
        wealth_deltas[unlucky_indices] = unlucky_amounts[unlucky_indices]
        wealth_deltas[lucky_indices] = lucky_amounts[lucky_indices]
        return StateUpdate(wealths=wealth_deltas)
