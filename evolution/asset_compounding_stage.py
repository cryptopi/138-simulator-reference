from config.evolution.asset_compounding_config import AssetCompoundingConfig
from evolution.evolution_stage import EvolutionStage
from simulation.state.simulation_state import SimulationState
from simulation.state.state_update import StateUpdate


class AssetCompoundingStage(EvolutionStage):

    def __init__(self, config: AssetCompoundingConfig):
        super().__init__()
        self.config = config

    def get_update(self, state: SimulationState, iteration: int) -> StateUpdate:
        compounding_percents = self.config.get_compound_percents(state)
        return StateUpdate(
            compounding_percents=compounding_percents
        )
