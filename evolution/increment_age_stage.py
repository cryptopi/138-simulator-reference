from config.evolution.increment_age_config import IncrementAgeConfig
from evolution.evolution_stage import EvolutionStage
from simulation.state.simulation_state import SimulationState
from simulation.state.state_update import StateUpdate

import numpy as np


class IncrementAgeStage(EvolutionStage):

    def __init__(self, config: IncrementAgeConfig):
        super().__init__()
        self.config = config

    def get_update(self, state: SimulationState, iteration: int) -> StateUpdate:
        return StateUpdate(
            ages=np.repeat(self.config.amount, state.agent_count)
        )
