from config.evolution.flat_tax_inheritance_config import \
    FlatTaxInheritanceConfig
from evolution.evolution_stage import EvolutionStage
from simulation.state.simulation_state import SimulationState
from simulation.state.state_update import StateUpdate

import numpy as np


class FlatTaxInheritanceStage(EvolutionStage):

    def __init__(self, config: FlatTaxInheritanceConfig):
        super().__init__()
        self.config = config
        self.refresh_percentile_interval = 30
        self.last_computed_percentile = -1
        self.tax_start = 0

    def get_update(self, state: SimulationState, iteration: int) -> StateUpdate:
        if self.last_computed_percentile == -1 or iteration - \
                self.last_computed_percentile >= \
                self.refresh_percentile_interval:
            self.tax_start = np.percentile(
                state.wealths,
                self.config.start_percentile
            )
            self.last_computed_percentile = iteration
            # print(f'Computed: {self.tax_start} for iteration {iteration}')

        death_ages = self.config.get_death_ages(
            state.agent_count
        )
        about_to_die = state.ages == death_ages
        dying_wealths = state.wealths[about_to_die]
        taxed = dying_wealths >= self.tax_start
        remaining_percentages = np.repeat(1, len(dying_wealths)).astype('float')
        remaining_percentages[taxed] = 1 - self.config.tax_percent / 100

        inheritances = dying_wealths * remaining_percentages
        revenue = np.sum(dying_wealths - inheritances)
        benefactors = state.children[about_to_die]
        delta_wealths = np.zeros(state.agent_count)
        delta_wealths[benefactors] += inheritances

        return StateUpdate(
            wealths=delta_wealths,
            is_taxable=False,
            inheritance_tax_revenue=revenue
        )