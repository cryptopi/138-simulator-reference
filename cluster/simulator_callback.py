from typing import Callable
from uuid import UUID

from simulation.simulation import Simulation
from simulation.state.simulation_state import SimulationState
from utils.utils import default


class SimulatorCallback:

    def __init__(
            self,
            on_iteration_complete: Callable[[UUID, UUID, int, SimulationState],
                                            None] = None,
            on_simulation_complete: Callable[[UUID, Simulation], None] = None
    ):
        self.on_iteration_complete = on_iteration_complete
        self.on_simulation_complete = on_simulation_complete
        self.on_iteration_complete = default(
            on_iteration_complete,
            lambda: 0,
            raw_backup=True
        )
        self.on_simulation_complete = default(
            on_simulation_complete,
            lambda: 0,
            raw_backup=True
        )
