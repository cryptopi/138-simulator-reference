from typing import Any
from uuid import UUID

from statistics.statistic import Statistic


class StatisticResultMessage:

    def __init__(
            self,
            statistic_id: UUID,
            simulation_id: UUID,
            job_id: str,
            iteration: int,
            result: Any
    ):
        self.statistic_id = statistic_id
        self.simulation_id = simulation_id
        self.job_id = job_id
        self.iteration = iteration
        self.result = result
