from uuid import UUID


class SimulationCompletedMessage:

    def __init__(self, job_id: UUID, simulation_id: UUID):
        self.group_id = job_id
        self.simulation_id = simulation_id
