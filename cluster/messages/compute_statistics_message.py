from typing import List

from simulation.state.simulation_state import SimulationState
from statistics.statistic import Statistic


class ComputeStatisticsMessage:

    def __init__(
            self,
            state: SimulationState,
            statistics: List[Statistic],
            group_id: str,
            iteration: int
    ):
        self.state = state
        self.statistics = statistics
        self.group_id = group_id
        self.iteration = iteration
