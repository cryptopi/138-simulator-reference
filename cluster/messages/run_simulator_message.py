from typing import List

from cluster.simulator import Simulator
from statistics.statistic import Statistic


class RunSimulatorMessage:

    def __init__(
            self,
            simulator: Simulator,
            statistics: List[Statistic],
            callback_type: str
    ):
        self.simulator = simulator
        self.statistics = statistics
        self.callback_type = callback_type
