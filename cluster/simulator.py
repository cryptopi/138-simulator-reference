from typing import List
from uuid import UUID

from cluster.simulator_callback import SimulatorCallback
from evolution.evolution_stage import EvolutionStage
from simulation.simulation import Simulation
from simulation.state.state_updater import StateUpdater
from utils.utils import DEBUG


class Simulator:

    def __init__(self,
                 job_id: UUID,
                 simulation: Simulation,
                 evolution_stages: List[EvolutionStage],
                 state_updater: StateUpdater,
    ):
        self.job_id = job_id
        self.simulation = simulation
        self.evolution_stages = evolution_stages
        self.state_updater = state_updater

    def simulate(self, callback: SimulatorCallback):
        self.simulation.initialize_initial_state()
        callback.on_iteration_complete(
            self.simulation.id,
            self.job_id,
            0,
            self.simulation.get_state(0)
        )
        if DEBUG:
            print('State 0:')
            print(self.simulation.get_state(0))
        iteration_count = self.simulation.config.evolution_config.iteration_count
        last_state = self.simulation.get_state(0)
        self.simulation.set_state(0, None)
        for i in range(iteration_count):
            updates = []
            # state = self.simulation.get_state(i)
            for stage in self.evolution_stages:
                updates.append(stage.get_update(last_state, (i + 1)))
            new_state = self.state_updater.update(last_state, updates)
            if DEBUG:
                print(f'State: {i + 1}')
                print(new_state)
            # self.simulation.set_state(i + 1, new_state)
            last_state = new_state
            callback.on_iteration_complete(
                self.simulation.id,
                self.job_id,
                (i + 1),
                new_state
            )
        # Print stuff out testing
        # last_state = self.simulation.get_state(iteration_count)
        # print(f'Total income tax revenue: {last_state.income_tax_revenue}')
        # print(f'Total inheritance tax revenue: {last_state.inheritance_tax_revenue}')
        callback.on_simulation_complete(self.job_id, self.simulation)


