from typing import List
from uuid import uuid4

from evolution.evolution_stage_factory import EvolutionStageFactory
from simulation.simulation import Simulation
from cluster.simulator import Simulator
from simulation.state.simulation_state_factory import SimulationStateFactory
from config.simulation_config import SimulationConfig
from simulation.state.state_updater import StateUpdater
from statistics.statistic import Statistic
from utils.utils import default


class SimulationJob:

    def __init__(self,
                 name: str,
                 simulation_config: SimulationConfig,
                 statistics: List[Statistic],
                 count: int,
                 params=None,
                 simulation_state_factory: SimulationStateFactory = None,
                 evolution_stage_factory: EvolutionStageFactory = None,
                 state_updater: StateUpdater = None):
        self.id = uuid4()
        self.name = name
        self.params = params
        self.simulation_config = simulation_config
        self.count = count
        self.statistics = statistics
        self.simulation_state_factory: SimulationStateFactory = default(
            simulation_state_factory,
            lambda: SimulationStateFactory()
        )
        self.evolution_stage_factory: EvolutionStageFactory = default(
            evolution_stage_factory,
            lambda: EvolutionStageFactory()
        )
        self.state_updater: StateUpdater = default(
            state_updater,
            lambda: StateUpdater()
        )

    def generate_simulators(self):
        evolution_stages = [
            self.evolution_stage_factory.create(stage)
            for stage in
            self.simulation_config.evolution_config.get_stage_configs()
        ]
        for i in range(self.count):
            simulation = Simulation(
                self.simulation_config,
                self.simulation_state_factory
            )
            simulator = Simulator(
                self.id,
                simulation,
                evolution_stages,
                self.state_updater
            )
            yield simulator
