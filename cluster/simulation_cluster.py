import queue
from collections import defaultdict
from itertools import chain
from typing import List, Iterable, Dict
from uuid import UUID
from os import getpid
from tqdm import tqdm

from cluster.messages.compute_statistics_message import ComputeStatisticsMessage
from cluster.messages.run_simulator_message import RunSimulatorMessage
from cluster.messages.simulation_completed_message import \
    SimulationCompletedMessage
from cluster.messages.statistic_result_message import StatisticResultMessage
from cluster.messages.terminate_process_message import TerminateProcessMessage
from cluster.simulation_job import SimulationJob
from multiprocessing import Process
from multiprocessing import Queue

from cluster.simulator_callback import SimulatorCallback
from statistics.statistic import Statistic
from utils.utils import DEBUG


def make_inline_iteration_callback(results_queue, statistics: List[Statistic]):
    def callback(simulation_id, group_id, iteration, state):
        for statistic in statistics:
            if statistic.should_compute(iteration):
                result = statistic.compute(state)
                results_queue.put(StatisticResultMessage(
                    statistic.id, simulation_id, group_id, iteration, result
                ))
    return callback


def make_simulation_callback(results_queue):
    def callback(job_id, simulation):
        results_queue.put(SimulationCompletedMessage(
            job_id,
            simulation.id
        ))
    return callback


def consume_work(
        work_queue: Queue,
        results_queue: Queue
):
    simulation_callback = make_simulation_callback(results_queue)
    while True:
        message = work_queue.get()
        if isinstance(message, RunSimulatorMessage):
            callback_type = message.callback_type
            if callback_type == SimulationCluster.INLINE_CALLBACK:
                iteration_callback = make_inline_iteration_callback(
                    results_queue,
                    message.statistics
                )
            else:
                raise ValueError(f'Unknown callback type {callback_type}')
            callback = SimulatorCallback(
                iteration_callback,
                simulation_callback
            )
            message.simulator.simulate(callback)
        elif isinstance(message, ComputeStatisticsMessage):
            pass
            # for statistic in message.statistics:
            #     result = statistic.compute(message.state)
            #     results_queue.put(StatisticResultMessage(
            #         statistic, message.group_id, message.iteration, result
            #     ))
        elif isinstance(message, TerminateProcessMessage):
            break
        else:
            raise ValueError(f'Work message {type(message)} not recognized')


class SimulationCluster:
    INLINE_CALLBACK = 'INLINE'

    def __init__(self, thread_count, min_buffer=10):
        self.thread_count = thread_count
        self.processes: List[Process] = []
        extra_capacity = int(max(min_buffer, self.thread_count * 0.2))
        self.work_queue: Queue = Queue(
            maxsize=self.thread_count + extra_capacity
        )
        self.results_queue: Queue = Queue()
        self.init_processes()

    def init_processes(self):
        for _ in range(self.thread_count):
            process = Process(
                target=consume_work,
                args=(self.work_queue, self.results_queue)
            )
            self.processes.append(process)

        for p in self.processes:
            p.start()

    def determine_callback_type(self, jobs: Iterable[SimulationJob]) -> str:
        return SimulationCluster.INLINE_CALLBACK

    def submit(self, *simulation_jobs: SimulationJob):
        print(f'Submitted on process {getpid()}')
        jobs_by_id: Dict[UUID, SimulationJob] = \
            {job.id: job for job in simulation_jobs}
        statistics_by_id: Dict[UUID, Statistic] = \
            {statistic.id: statistic for job in simulation_jobs for statistic
             in job.statistics}
        callback_type = self.determine_callback_type(simulation_jobs)
        simulator_generators = []
        for job in simulation_jobs:
            generator = job.generate_simulators()
            # zipped = zip([3], generator)
            # simulator_generators.append(zipped)
            simulator_generators.append(generator)
        simulator_generator = chain(*simulator_generators)

        def try_add_simulator(simulator):
            job = jobs_by_id[simulator.job_id]
            message = RunSimulatorMessage(
                simulator,
                job.statistics,
                callback_type
            )
            try:
                self.work_queue.put(message, block=False)
                return True
            except queue.Full:
                return False

        leftover = None
        for simulator in simulator_generator:
            added = try_add_simulator(simulator)
            if not added:
                leftover = simulator
                break

        if leftover is not None:
            simulator_generator = chain([leftover], simulator_generator)

        statistic_result_counts = {}
        for job in simulation_jobs:
            iteration_list = list(
                range(
                    job.simulation_config.evolution_config.iteration_count + 1
                )
            )
            for statistic in job.statistics:
                count = len([x for x in iteration_list if
                            statistic.should_compute(x)])
                statistic_result_counts[statistic.id] = count

        remaining_statistics = set()
        for job in simulation_jobs:
            for statistic in job.statistics:
                remaining_statistics.add((job.id, statistic.id))

        simulation_statistic_iteration_count = sum([
            statistic_result_counts[statistic.id]
            for job in simulation_jobs for statistic in job.statistics
        ])
        progress_bar = tqdm(
            total=simulation_statistic_iteration_count,
            position=0
        )

        computed_statistics = {}
        finalized_statistics = defaultdict(list)
        while True:
            message = self.results_queue.get()
            if isinstance(message, StatisticResultMessage):
                if not DEBUG:
                    progress_bar.update(1)
                key = (
                    message.job_id,
                    message.statistic_id
                )
                if key not in computed_statistics:
                    computed_statistics[key] = {
                        'simulations': defaultdict(dict),
                        'completed': set()
                    }
                job = jobs_by_id[message.job_id]
                statistic = statistics_by_id[message.statistic_id]

                simulation_statistic_results = \
                    computed_statistics[key]['simulations'][message.simulation_id]
                simulation_statistic_results[message.iteration] = message.result

                target_result_count = statistic_result_counts[statistic.id]
                actual_result_count = len(simulation_statistic_results)
                completed_simulations = computed_statistics[key]['completed']
                # The statistic for this particular simulation is done
                if target_result_count == actual_result_count:
                    completed_simulations.add(message.simulation_id)

                target_simulation_count = job.count
                actual_simulation_count = len(completed_simulations)
                # The statistic for this entire job is done
                if target_simulation_count == actual_simulation_count:
                    is_multi_group = target_simulation_count > 1
                    results = {}
                    if is_multi_group:
                        simulations = computed_statistics[key]['simulations']
                        to_group = defaultdict(dict)
                        for simulation_id in simulations:
                            result_dict = simulations[simulation_id]
                            for iteration in result_dict:
                                to_group[iteration][simulation_id] = \
                                    result_dict[iteration]
                        for iteration in to_group:
                            iteration_results = to_group[iteration]
                            grouped = statistic.aggregate(iteration_results)
                            results[iteration] = grouped
                    else:
                        results = simulation_statistic_results
                    key = (job.name, statistic.name)
                    finalized_statistics[key].append((job.params, results))
                    remaining_statistics.remove((job.id, statistic.id))
                    if len(remaining_statistics) == 0:
                        progress_bar.close()
                        return finalized_statistics
            elif isinstance(message, SimulationCompletedMessage):
                if callback_type == SimulationCluster.INLINE_CALLBACK:
                    simulator_to_add = next(simulator_generator, None)
                    if simulator_to_add is not None:
                        added = try_add_simulator(simulator_to_add)
                        if not added:
                            print('Not added!')
                            simulator_generator = chain(
                                [simulator_to_add],
                                simulator_generator
                            )

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.tear_down()

    def tear_down(self):
        for p in self.processes:
            p.terminate()
        for p in self.processes:
            p.join()
        for p in self.processes:
            p.close()
