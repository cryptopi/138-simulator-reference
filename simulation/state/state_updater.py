from typing import List
import numpy as np

from utils.utils import DEBUG, to_chunked_str
from .simulation_state import SimulationState
from .state_update import StateUpdate


class StateUpdater:

    def __init__(self):
        pass

    def update(self, state: SimulationState, updates: List[StateUpdate]):
        copy = state.copy()
        state_replace_updates = []
        age_update = None
        compounding_update = None
        income_tax_update = None
        taxable_wealth_deltas = np.zeros(state.agent_count)
        for update in updates:
            if update.talents is not None:
                copy.talents += update.talents
            if update.wealths is not None:
                if DEBUG:
                    print(f'Wealth changes')
                    print(to_chunked_str(update.wealths, 20))
                if update.is_taxable:
                    taxable_wealth_deltas += update.wealths
                else:
                    copy.wealths += update.wealths
            if update.p_unluckies is not None:
                copy.p_unluckies += update.p_unluckies
            if update.p_luckies is not None:
                copy.p_luckies += update.p_luckies
            if update.state_replace is not None:
                state_replace_updates.append(update)
            if update.surpluses is not None:
                # TODO maybe use surplus info for something
                pass
            if update.ages is not None:
                age_update = update
            if update.compounding_percents is not None:
                if compounding_update is not None:
                    compounding_update.compounding_percents += \
                        update.compounding_percents
                else:
                    compounding_update = update
            if update.income_tax_rates is not None:
                if income_tax_update is not None:
                    raise ValueError('Can only have one income tax per '
                                     'iteration')
                income_tax_update = update
            if update.inheritance_tax_revenue is not None:
                copy.inheritance_tax_revenue += update.inheritance_tax_revenue

        if compounding_update is not None:
            returns = (compounding_update.compounding_percents / 100) * \
                       state.wealths
            taxable_wealth_deltas += returns

        if income_tax_update is not None:
            tax_revenue = taxable_wealth_deltas * income_tax_update.income_tax_rates
            tax_revenue = np.maximum(tax_revenue, 0)
            taxable_wealth_deltas -= tax_revenue
            copy.income_tax_revenue += np.sum(tax_revenue)

        copy.wealths += taxable_wealth_deltas

        if len(state_replace_updates) > 0:
            if len(state_replace_updates) > 1:
                print(f'Warning: more than one {len(state_replace_updates)} '
                      f'state replace updates detected. Might result in '
                      f'undefined behavior.')
            for update in state_replace_updates:
                if update.parent_indices is not None:
                    child_index_values = np.arange(copy.agent_count)[
                        update.indices]
                    copy.children[update.parent_indices] = child_index_values
                    copy.merge(update.state_replace, update.indices)

        if age_update is not None:
            copy.ages += age_update.ages

        return copy
