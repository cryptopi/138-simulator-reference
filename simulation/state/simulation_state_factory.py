from config.breeding.simple_breeding_config import SimpleBreedingConfig
from config.state.state_config import StateConfig
from simulation.state.simulation_state import SimulationState

import numpy as np


class SimulationStateFactory:

    def __init__(self):
        pass

    def generate_count(self, state_config: StateConfig, count):
        wealths = state_config.initial_wealth_distribution.generate(
            count
        ).astype('float64')
        talents = state_config.talent_distribution.generate(count) \
            .astype('float64')
        p_unluckies = state_config.p_unlucky_distribution.generate(
            count
        ).astype('float64')
        p_luckies = state_config.p_lucky_distribution.generate(
            count
        ).astype('float64')
        ages = state_config.age_distribution.generate(
            count
        ).astype('int')
        return SimulationState(
            wealths=wealths,
            talents=talents,
            p_unluckies=p_unluckies,
            p_luckies=p_luckies,
            generations=np.zeros(count),
            children=np.repeat(-1, count),
            ages=ages,
            agent_count=count,
            income_tax_revenue=0,
            inheritance_tax_revenue=0
        )

    def sequence_generations(self, generation_states) -> SimulationState:
        # First generation is first in array
        state = SimulationState.empty(0)
        # Set children to point to those from the next generation and concat
        for generation_state in generation_states[:-1]:
            generation_state.children = state.agent_count + \
                generation_state.agent_count + \
                np.arange(generation_state.agent_count)
            state = state.concat(generation_state)
        state = state.concat(generation_states[-1])

        return state

    def create(self, state_config: StateConfig) -> SimulationState:
        agent_count = state_config.agent_count
        if isinstance(state_config.breeding_config, SimpleBreedingConfig):
            config: SimpleBreedingConfig = state_config.breeding_config
            count = int(agent_count / config.multiplier)
            generation_state = self.generate_count(state_config, count)
            generation_states = [generation_state]
            for i in range(1, config.multiplier):
                child_state = config.child_deriver.derive_children(
                    generation_state
                )
                generation_states.append(child_state)
                generation_state = child_state
            state = self.sequence_generations(generation_states)
        else:
            state = self.generate_count(state_config, agent_count)

        return state
