import numpy as np

from utils.utils import to_chunked_str


class SimulationState:

    def __init__(
            self,
            wealths,
            talents,
            p_unluckies,
            p_luckies,
            generations,
            children,
            ages,
            agent_count,
            income_tax_revenue,
            inheritance_tax_revenue
    ):
        self.wealths = wealths
        self.talents = talents
        self.p_unluckies = p_unluckies
        self.p_luckies = p_luckies
        self.generations = generations
        self.children = children
        self.ages = ages
        self.agent_count = agent_count
        self.income_tax_revenue = income_tax_revenue
        self.inheritance_tax_revenue = inheritance_tax_revenue

    @staticmethod
    def empty(agent_count):
        return SimulationState(
            np.empty(agent_count),
            np.empty(agent_count),
            np.empty(agent_count),
            np.empty(agent_count),
            np.empty(agent_count),
            np.empty(agent_count),
            np.empty(agent_count),
            agent_count,
            0,
            0
        )

    def slice(self, indices):
        return SimulationState(
            wealths=self.wealths[indices],
            talents=self.talents[indices],
            p_unluckies=self.p_unluckies[indices],
            p_luckies=self.p_luckies[indices],
            generations=self.generations[indices],
            children=self.children[indices],
            ages=self.ages[indices],
            agent_count=np.sum(indices),
            income_tax_revenue=self.income_tax_revenue,
            inheritance_tax_revenue=self.inheritance_tax_revenue
        )

    def copy(self):
        return SimulationState(wealths=np.copy(self.wealths),
                               talents=np.copy(self.talents),
                               p_unluckies=np.copy(self.p_unluckies),
                               p_luckies=np.copy(self.p_luckies),
                               generations=np.copy(self.generations),
                               children=np.copy(self.children),
                               ages=np.copy(self.ages),
                               agent_count=self.agent_count,
                               income_tax_revenue=self.income_tax_revenue,
                               inheritance_tax_revenue=self.inheritance_tax_revenue
                               )

    def merge(self, other, indices):
        if indices is None:
            indices = np.repeat(True, self.agent_count)
        if len(other.wealths) != np.sum(indices):
            raise ValueError('Merge lengths unequal')
        self.wealths[indices] = other.wealths
        self.talents[indices] = other.talents
        self.p_unluckies[indices] = other.p_unluckies
        self.p_luckies[indices] = other.p_luckies
        self.generations[indices] = other.generations
        self.children[indices] = other.children
        self.ages[indices] = other.ages
        # TODO should merge revenues or not?

    def concat(self, other):
        return SimulationState(
            wealths=np.append(self.wealths, other.wealths),
            talents=np.append(self.talents, other.talents),
            p_unluckies=np.append(self.p_unluckies, other.p_unluckies),
            p_luckies=np.append(self.p_luckies, other.p_luckies),
            generations=np.append(self.generations, other.generations),
            children=np.append(self.children, other.children).astype('int'),
            ages=np.append(self.ages, other.ages),
            agent_count=self.agent_count + other.agent_count,
            income_tax_revenue=self.income_tax_revenue + other.income_tax_revenue,
            inheritance_tax_revenue=self.inheritance_tax_revenue +
                                    other.inheritance_tax_revenue
        )

    def to_string(self, name, arr, chunk):
        result = f'{name:}\n'
        result += to_chunked_str(arr, chunk)
        result += '\n'
        return result

    def __str__(self):
        result = ''
        LENGTH = 20
        result += self.to_string('Wealths', self.wealths, LENGTH)
        result += self.to_string('Talents', self.talents, LENGTH)
        result += self.to_string('Prob Unlucky', self.p_unluckies, LENGTH)
        result += self.to_string('Prob Lucky', self.p_luckies, LENGTH)
        result += self.to_string('Generations', self.generations, LENGTH)
        result += self.to_string('Children', self.children, LENGTH)
        result += self.to_string('Ages', self.ages, LENGTH)
        result += f'Agent count: {self.agent_count}\n'
        result += f'Income tax revenue: {self.income_tax_revenue}\n'
        result += f'Inheritance tax revenue: {self.inheritance_tax_revenue}\n'
        return result


