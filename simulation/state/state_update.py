

class StateUpdate:

    def __init__(self,
                 wealths=None,
                 is_taxable=True,
                 talents=None,
                 p_unluckies=None,
                 p_luckies=None,
                 state_replace=None,
                 parent_indices=None,
                 indices=None,
                 surpluses=None,
                 ages=None,
                 compounding_percents=None,
                 income_tax_rates=None,
                 inheritance_tax_revenue=None
     ):
        self.wealths = wealths
        self.is_taxable = is_taxable
        self.talents = talents
        self.p_unluckies = p_unluckies
        self.p_luckies = p_luckies
        self.state_replace = state_replace
        self.parent_indices = parent_indices
        self.indices = indices
        self.surpluses = surpluses
        self.ages = ages
        self.compounding_percents = compounding_percents
        self.income_tax_rates = income_tax_rates
        self.inheritance_tax_revenue = inheritance_tax_revenue
