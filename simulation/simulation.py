from typing import List
from uuid import uuid4

from config.simulation_config import SimulationConfig
from simulation.state.simulation_state import SimulationState
from simulation.state.simulation_state_factory import SimulationStateFactory


class Simulation:

    def __init__(self,
                 config: SimulationConfig,
                 simulation_state_factory: SimulationStateFactory
                 ):
        self.id = uuid4()
        self.config: SimulationConfig = config
        self.states: List[SimulationState] = \
            [None] * (self.config.evolution_config.iteration_count + 1)
        self.simulation_state_factory = simulation_state_factory

    def initialize_initial_state(self):
        self.states[0] = self.simulation_state_factory.create(self.config.state_config)

    def get_state(self, iteration: int) -> SimulationState:
        return self.states[iteration]

    def set_state(self, iteration: int, state: SimulationState):
        self.states[iteration] = state
